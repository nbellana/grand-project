const mongoose = require("mongoose");

const productSchema = mongoose.Schema({
  product_name: {
    type: String,
    required: true,
  },
  brand_name: {
    type: String,
    required: true,
  },
  price: {
    type: String,
    required: true,
  },
  sizes_available: {
    type: String,
    required: true,
  },
  quantity: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("Product", productSchema);
