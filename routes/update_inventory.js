//External imports
const express = require("express");
const router = express.Router();

//Internal imports
const Product = require("../models/ProductModel");

//Showing the product details with the help of id
router.get("/update_inventory/:id", async (req, res) => {
  try {
    const product = await Product.findById(req.params.id);
    res.json(product);
  } catch (err) {
    res.status(404).json({ Error: "Oops! product not found." });
  }
});

//Updating the product with the help of id
router.put("/update_inventory/:id", async (req, res) => {
  Product.findByIdAndUpdate(
    { _id: req.params.id },
    {
      $set: {
        product_name: req.body.product_name,
        brand_name: req.body.brand_name,
        price: req.body.price,
        number: req.body.number,
        gender: req.body.gender,
      },
    }
  )
    .then((result) => {
      res.status(200).json({ updated_product: result });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

//Exporting the module
module.exports = router;
