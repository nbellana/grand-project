//External imports
const express = require("express");
const router = express.Router();

//Internal imports
const Product = require("../models/ProductModel");

//Get request to view all products in the inventory
router.get("/add_to_inventory", async (req, res) => {
  try {
    const products = await Product.find();
    res.json(products);
  } catch (err) {
    res.send("Error" + err);
  }
});

//Creating a product in the inventory
router.post("/", async (req, res) => {
  const product = new Product({
    product_name: req.body.product_name,
    brand_name: req.body.brand_name,
    price: req.body.price,
    sizes_available: req.body.sizes_available,
    quantity: req.body.quantity,
  });
  try {
    const p = await product.save();
    res.json(p);
  } catch (err) {
    res.send("Error");
  }
});

//Exporting the module
module.exports = router;
