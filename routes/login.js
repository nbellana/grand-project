//External imports
const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");

//Internal impports
const User = require("../models/UserModel");

router.post("/login", async (req, res) => {
  const { number, password } = req.body;

  if (!number || !password) {
    return res.status(400).json({ msg: "Please provide name and password" });
  }

  //check if the user is present or not
  const user = await User.findOne({ number });
  if (!user) {
    return res
      .status(404)
      .json({ msg: "Given mobile number and password are not matching" });
  }

  //Compare the user entered password with the password in the database
  bcrypt.compare(password, user.password, function (err, result) {
    if (result == true) {
      res.status(200).json({
        name: user.name,
        email: user.email,
        number,
        password: true,
        status: "active",
        login_time: new Date(),
      });
    } else if (result === false) {
      res.status(401).json({
        msg: "Incorrect password/Unauthorized user",
        password: false,
        status: "inactive",
      });
    } else {
      res
        .status(500)
        .json({ msg: "An error occured at our side please try again later" });
    }
  });
});

//Exporting the module
module.exports = router;
