//External imports
const express = require("express");
const router = express.Router();

//Internal imports
const Product = require("../models/ProductModel");

//Get request for viewing all the products in the inventory
router.get("/view_products", async (req, res) => {
  try {
    const products = await Product.find();
    res.json(products);
  } catch (err) {
    res.status(400).json({ Error: "Bad request" });
  }
});

//Get request for viewing a single product with the help of id
router.get("/view_products/:id", async (req, res) => {
  try {
    const product = await Product.findById(req.params.id);
    res.json(product);
  } catch (err) {
    res.status(404).json({ Error: "Oops! product not found." });
  }
});

//Exporting the module
module.exports = router;
