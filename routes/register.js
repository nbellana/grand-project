//External imports
const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");

//Internal imports
const User = require("../models/UserModel");

router.post("/register", async (req, res) => {
  const { name, email, password, number, gender } = req.body;

  //Check if any required fields are left empty
  if (!name || !email || !password || !number || !gender) {
    return res.status(400).json({ msg: "Please provide all required fileds" });
  }

  //   check duplicate emails
  const emailAlreadyExist = await User.findOne({ email });
  if (emailAlreadyExist) {
    return res.status(409).json({ msg: "Email already exist" });
  }
  //Password encryption function
  bcrypt.hash(req.body.password, 10, async function (err, hash) {
    if (err) {
      return res
        .status(500)
        .json({ msg: "An error occured at our side please try again later" });
    }
    //Creates a new user if he doesn't exist already.
    const user = await User.create({
      name,
      email,
      password: hash,
      number,
      gender,
    });
    res.status(200).json(user);
  });
});

//Displays all the registerd users
router.get("/register", async (req, res) => {
  try {
    const users = await User.find();
    res.json(users);
  } catch (err) {
    res.send("Error" + err);
  }
});

//Exporting the module
module.exports = router;
