//External imports
const express = require("express");
const router = express.Router();

//Internal imports
const Product = require("../models/ProductModel");

//Searching a product with product name
router.get("/search_product", async (req, res) => {
  try {
    var searchProductName = req.body.searchProductName;
    var product_data = await Product.find({
      // regex expression to query the product name
      product_name: { $regex: ".*" + searchProductName + ".*" },
    });
    if (product_data.length > 0) {
      res
        .status(200)
        .send({ success: true, msg: "Product details", data: product_data });
    } else {
      res.status(404).send({ msg: "Product not found" });
    }
  } catch (err) {
    res.status(400).send({ success: false, msg: err.message });
  }
});

//Exporting the module
module.exports = router;
