//External imports
const express = require("express");
const mongoose = require("mongoose");

//Constants
const URL = "mongodb://localhost/myntra";
const PORT = "8000";

//Creating and defining express application
const app = express();

//Connecting to the database
mongoose.connect(URL, { useNewUrlParser: true });
const con = mongoose.connection;

//If the connection is open console connected
con.on("open", () => {
  console.log("connected!");
});

//Telling the express application to use json
app.use(express.json());

//Importing the add_to_inventory route and telling the application to use this route
const addProductsRouter = require("./routes/add_to_inventory");
app.use("/", addProductsRouter);

//Importing the view_products route and telling the application to use this route
const viewProductsRouter = require("./routes/view_products");
app.use("/", viewProductsRouter);

//Importing the update_inventory route and telling the application to use this route
const updateProductRouter = require("./routes/update_inventory");
app.use("/", updateProductRouter);

//Importing the search_product route and telling the application to use this route
const searchProductRouter = require("./routes/search_product");
app.use("/", searchProductRouter);

//Importing the login route and telling the application to use this route
const loginRouter = require("./routes/login");
app.use("/", loginRouter);

//Importing the register route and telling the application to use this route
const registerRouter = require("./routes/register");
app.use("/", registerRouter);

//On successful connection try listening to the port
app.listen(PORT, () => {
  console.log(`Server started running on ${PORT}!`);
});
